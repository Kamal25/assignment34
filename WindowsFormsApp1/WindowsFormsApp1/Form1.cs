﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Inderpreet Kaur c0739297
            //Amandeep Singh c0740035
            //Kamalpreet Kaur Brar c0738809
            double distance;
            double baseFare = 2.50; ;
            double disCharge = 0.81; ;
            double serFee = 1.75;
            double total;
            double minFare = 5.50;



            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("please enter from and to location");
            }

            if (radioButton1.Checked == false && radioButton2.Checked == false)
            {
                MessageBox.Show("please select pool or direct");
            }
            if (radioButton2.Checked == true)
            {
                baseFare = baseFare + 0.25;
                disCharge = disCharge + 0.12;

            }




            if (textBox1.Text == "275 Yorkland Blvd" && textBox2.Text == "CN Tower")
            {

                distance = 22.9;

                disCharge = disCharge * distance;
                disCharge = Math.Round(disCharge, 2);

                total = baseFare + disCharge + serFee;

                if (total < minFare)
                {
                    total = minFare;
                }

                total = Math.Round(total, 2);

                MessageBox.Show("Ryde Pool Confirmed \n" + "From:" + textBox1.Text + "\n" + "To:" + textBox2.Text + "\n" + "" + "\n" + "Booking Fee: $" +
                                baseFare + "\n" + "Distance Charge: $" + disCharge + "\n" + "ServiceFee: $" + serFee + "\n" + "" + "\n" + "" + "\n" + "Total: $" + total);

            }

            if (textBox1.Text == "Fairview Mall" && textBox2.Text == "Pizza Pizza")
            {


                distance = 1.2;
                disCharge = disCharge * distance;
                disCharge = Math.Round(disCharge, 2);
                total = baseFare + disCharge + serFee;

                if (total < minFare)
                {
                    total = minFare;
                }

                total = Math.Round(total, 2);
                MessageBox.Show("Ryde Pool Confirmed \n" + "From:" + textBox1.Text + "\n" + "To:" + textBox2.Text + "\n" + "" + "\n" + "Booking Fee: $" +
                                 baseFare + "\n" + "Distance Charge: $" + disCharge + "\n" + "ServiceFee: $" + serFee + "\n" + "" + "\n" + "" + "\n" + "Total: $" + total);

            }

            if (textBox1.Text != "275 Yorkland Blvd" || textBox2.Text != "CN Tower")
            {
                MessageBox.Show("please enter valid from and to location");
            }
            else if (textBox1.Text != "Fairview Mall" || textBox2.Text != "Pizza Pizza")
            {
                MessageBox.Show("please enter valid from and to location");
            }




            else
            {
                textBox1.Text = "";
                textBox2.Text = "";
            }

        }
    }
}
